package postgresutils

import (
	"fmt"
	"strings"
)

func PlaceholdersStartingAt(startingAt int, number int) string {
	if startingAt < 1 {
		panic("startingAt must be positive")
	}
	if number < 1 {
		panic("number must be positive")
	}
	placeholders := make([]string, number)
	for placeholderIndex := startingAt; placeholderIndex < startingAt+number; placeholderIndex++ {
		placeholders[placeholderIndex-startingAt] = fmt.Sprintf("$%d", placeholderIndex)
	}
	return strings.Join(placeholders, ", ")
}

func InsertValues(columnsNum int, rowsNum int) string {
	return InsertValuesStartingAt(1, columnsNum, rowsNum)
}

func InsertValuesStartingAt(startingAt int, columnsNum int, rowsNum int) string {
	if startingAt < 1 {
		panic("startingAt must be positive")
	}
	if columnsNum < 1 {
		panic("columnsNum must be positive")
	}
	if rowsNum < 1 {
		panic("rowsNum must be positive")
	}
	rowsStrings := make([]string, rowsNum)
	for rowIndex := 0; rowIndex < rowsNum; rowIndex++ {
		placeholders := PlaceholdersStartingAt(startingAt+rowIndex*columnsNum, columnsNum)
		rowsStrings[rowIndex] = fmt.Sprintf("(%s)", placeholders)
	}
	return strings.Join(rowsStrings, ", ")
}

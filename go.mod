module gitlab.com/witchbrew/go/postgresutils

go 1.14

require (
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fasttemplate v1.2.1 // indirect
	gitlab.com/witchbrew/go/sqltemplate v0.0.0-20200811214237-570b956c70b0 // indirect
)

package postgresutils

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPlaceholdersStartingAt(t *testing.T) {
	expected := "$4, $5, $6"
	actual := PlaceholdersStartingAt(4, 3)
	require.Equal(t, expected, actual)
}

func TestPlaceholdersStartingAtBadStartingAt(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "startingAt must be positive", r)
	}()
	PlaceholdersStartingAt(-1, 1)
}

func TestPlaceholdersStartingAtBadNumber(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "number must be positive", r)
	}()
	PlaceholdersStartingAt(1, -1)
}

func TestInsertValuesStartingAt(t *testing.T) {
	expected := "($2, $3), ($4, $5), ($6, $7)"
	actual := InsertValuesStartingAt(2, 2, 3)
	require.Equal(t, expected, actual)
}

func TestInsertValuesStartingAtBadStartingAt(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "startingAt must be positive", r)
	}()
	InsertValuesStartingAt(-1, 1, 1)
}

func TestInsertValuesBadColumnsNum(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "columnsNum must be positive", r)
	}()
	InsertValues(-1, 1)
}

func TestInsertValuesBadRowsNum(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "rowsNum must be positive", r)
	}()
	InsertValues(1, -1)
}
